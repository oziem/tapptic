package com.example.tapptic

import com.example.datasource.model.Item
import com.example.datasource.repository.ApiError
import com.example.datasource.repository.ApiRepository
import com.example.tapptic.list.ListPresenter
import com.example.tapptic.list.ListViewState
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class ListPresenterTest {
    @Rule
    @JvmField
    val overrideSchedulersRule = RxSchedulersOverrideRule()

    @Mock
    private lateinit var apiRepository: ApiRepository

    private lateinit var presenter: ListPresenter

    private lateinit var viewStateTestObservable: TestObserver<ListViewState>

    @Before
    fun init() {
        MockitoAnnotations.initMocks(this)
        presenter = ListPresenter(apiRepository)
        viewStateTestObservable = presenter.stateObservable.test()
            .apply { onComplete() }
    }

    @Test
    fun `Should show empty state`() {
        // given
        val items = listOf<Item>()
        Mockito.`when`(apiRepository.getItems()).thenReturn(Single.just(items))

        // when
        presenter.fetchItems()

        // then
        viewStateTestObservable.assertResult(
            ListViewState.Loading,
            ListViewState.Empty
        )
    }

    @Test
    fun `Should show populated state`() {
        // given
        val items = listOf(Item("name", "url"))
        Mockito.`when`(apiRepository.getItems()).thenReturn(Single.just(items))

        // when
        presenter.fetchItems()

        // then
        viewStateTestObservable.assertResult(
            ListViewState.Loading,
            ListViewState.Populated(items)
        )
    }

    @Test
    fun `Should show error state with NoNetworkConnection`() {
        // given
        val exception = Exception()
        Mockito.`when`(apiRepository.getItems()).thenReturn(Single.error(exception))
        Mockito.`when`(apiRepository.parseError(exception)).thenReturn(ApiError.NoNetworkConnection)

        // when
        presenter.fetchItems()

        // then
        viewStateTestObservable.assertResult(
            ListViewState.Loading,
            ListViewState.Error(ApiError.NoNetworkConnection)
        )
    }

    @Test
    fun `Should show error state with UnknownError`() {
        // given
        val exception = Exception()
        val info = "info"
        Mockito.`when`(apiRepository.getItems()).thenReturn(Single.error(exception))
        Mockito.`when`(apiRepository.parseError(exception)).thenReturn(ApiError.UnknownError(info))

        // when
        presenter.fetchItems()

        // then
        viewStateTestObservable.assertResult(
            ListViewState.Loading,
            ListViewState.Error(ApiError.UnknownError(info))
        )
    }
}
