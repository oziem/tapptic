package com.example.tapptic.common

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign

abstract class MvpActivity<S: Any, P: MvpPresenter<S>> : AppCompatActivity() {

    private val compositeDisposable = CompositeDisposable()

    lateinit var presenter: P

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        attachPresenter()
    }

    override fun onRetainCustomNonConfigurationInstance() = presenter

    override fun onDestroy() {
        compositeDisposable.dispose()
        if (isFinishing) {
            presenter.destroy()
        }
        super.onDestroy()
    }

    @Suppress("UNCHECKED_CAST")
    private fun attachPresenter() {
        presenter = lastCustomNonConfigurationInstance as? P
            ?: createPresenter()
        compositeDisposable += presenter.stateObservable.subscribe(::onStateChanged)
    }

    abstract fun createPresenter(): P

    abstract fun onStateChanged(state: S)
}
