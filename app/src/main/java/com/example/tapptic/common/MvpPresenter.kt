package com.example.tapptic.common

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.BehaviorSubject

abstract class MvpPresenter<S: Any> {

    protected val compositeDisposable = CompositeDisposable()
    protected val statePublisher = BehaviorSubject.create<S>()
    val stateObservable: Observable<S> = statePublisher
        .observeOn(AndroidSchedulers.mainThread())

    fun destroy() {
        compositeDisposable.dispose()
    }
}
