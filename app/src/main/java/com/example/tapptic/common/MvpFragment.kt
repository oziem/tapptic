package com.example.tapptic.common

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign

abstract class MvpFragment<S: Any, P: MvpPresenter<S>> : Fragment() {

    private val compositeDisposable = CompositeDisposable()

    lateinit var presenter: P

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = createPresenter()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        compositeDisposable += presenter.stateObservable.subscribe(::onStateChanged)
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
        presenter.destroy()
        super.onDestroy()
    }

    abstract fun createPresenter(): P

    abstract fun onStateChanged(state: S)
}
