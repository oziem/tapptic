package com.example.tapptic.common

import android.support.v4.app.Fragment

fun Fragment.requireArguments() = requireNotNull(this.arguments) { "Fragment arguments were null" }
