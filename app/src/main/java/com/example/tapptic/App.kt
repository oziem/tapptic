package com.example.tapptic

import android.app.Application
import android.content.Context
import com.example.tapptic.di.AppComponent
import com.example.tapptic.di.ApplicationModule
import com.example.tapptic.di.DaggerAppComponent

class App : Application() {
    companion object {
        fun getComponent(context: Context) = (context.applicationContext as App).appComponent

        @JvmStatic
        fun getPicasso(context: Context) = getComponent(context).getPicasso()
    }

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent
            .builder()
            .applicationModule(ApplicationModule(this))
            .build()
    }
}
