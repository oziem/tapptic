package com.example.tapptic.list

import com.example.datasource.model.Item
import com.example.datasource.repository.ApiError
import com.example.datasource.repository.ApiRepository
import com.example.tapptic.common.MvpPresenter
import io.reactivex.rxkotlin.plusAssign
import javax.inject.Inject

class ListPresenter @Inject constructor(
    private val apiRepository: ApiRepository
) : MvpPresenter<ListViewState>() {

    fun fetchItems() {
        compositeDisposable += apiRepository.getItems()
            .toObservable()
            .map {
                if (it.isEmpty()) {
                    ListViewState.Empty
                } else {
                    ListViewState.Populated(it)
                }
            }
            .onErrorReturn { ListViewState.Error(apiRepository.parseError(it)) }
            .startWith(ListViewState.Loading)
            .subscribe(statePublisher::onNext)
    }
}

sealed class ListViewState {
    object Loading : ListViewState()
    object Empty : ListViewState()
    data class Error(val error: ApiError) : ListViewState()
    data class Populated(val items: List<Item>) : ListViewState()
}
