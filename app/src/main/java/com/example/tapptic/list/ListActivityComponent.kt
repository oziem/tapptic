package com.example.tapptic.list

import com.example.tapptic.di.ActivityScope
import dagger.Subcomponent

@ActivityScope
@Subcomponent
interface ListActivityComponent {
    fun getPresenter(): ListPresenter
}
