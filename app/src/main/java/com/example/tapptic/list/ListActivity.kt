package com.example.tapptic.list

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.example.datasource.model.Item
import com.example.datasource.repository.ApiError
import com.example.tapptic.App
import com.example.tapptic.R
import com.example.tapptic.common.MvpActivity
import com.example.tapptic.details.ItemDetailsActivity
import com.example.tapptic.details.fragment.ItemDetailFragment
import kotlinx.android.synthetic.main.activity_list.*
import kotlinx.android.synthetic.main.list_view.*

class ListActivity : MvpActivity<ListViewState, ListPresenter>() {

    private val component by lazy {
        App.getComponent(this).listActivityComponent()
    }

    private val items = mutableListOf<Item>()
    private var fragment: ItemDetailFragment? = null
    private lateinit var adapter: ListAdapter

    override fun createPresenter() = component.getPresenter().apply {
        fetchItems()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        ItemDetailFragment.readDetailsFromState(savedInstanceState)?.let { openDetailsActivity(it) }
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        if (detailsContainer == null) {
            removeFragment()
        }
        recyclerView.adapter = ListAdapter(items, ::goToDetails).apply {
            adapter = this
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == DETAILS_ACTIVITY_CODE &&
            resultCode == Activity.RESULT_OK &&
            data?.extras != null
        ) {
            data.getStringExtra(ItemDetailsActivity.ITEM_NAME_EXTRA).let {
                adapter.setSelectedByName(it)
                showDetailsInSecondPane(it)
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        val resultState = outState.apply { fragment?.saveState(this) }
        super.onSaveInstanceState(resultState)
    }

    override fun onStateChanged(state: ListViewState) = when (state) {
        ListViewState.Loading -> showLoading()
        ListViewState.Empty -> showError(getString(R.string.there_is_no_content))
        is ListViewState.Error -> onErrorState(state.error)
        is ListViewState.Populated -> onPopulatedState(state.items)
    }

    private fun showLoading(show: Boolean = true) {
        progressBar.visibility =
            if (show) View.VISIBLE
            else View.GONE
    }

    private fun showError(message: String) {
        clearView()
        retryButton.visibility = View.VISIBLE
        with(errorText) {
            visibility = View.VISIBLE
            text = message
        }
    }

    private fun onErrorState(error: ApiError) {
        showError(
            when (error) {
                ApiError.NoNetworkConnection -> getString(R.string.no_network_connection)
                is ApiError.UnknownError -> getString(R.string.unknown_error, null)
            }
        )
    }

    private fun onPopulatedState(items: List<Item>) {
        clearView()
        this.items.clear()
        this.items.addAll(items)
        adapter.notifyDataSetChanged()
        returnToSelection()
    }

    private fun returnToSelection() {
        fragment?.getDetailedItemName()?.let {
            val position = adapter.setSelectedByName(it)
            recyclerView.scrollToPosition(position)
        }
    }

    private fun clearView() {
        showLoading(false)
        errorText.visibility = View.GONE
        retryButton.visibility = View.GONE
    }

    private fun goToDetails(itemName: String) {
        if (detailsContainer == null) {
            openDetailsActivity(itemName)
        } else {
            showDetailsInSecondPane(itemName)
        }
    }

    fun onRetryClick(view: View) {
        presenter.fetchItems()
    }

    private fun openDetailsActivity(itemName: String) {
        ItemDetailsActivity.start(this, itemName, DETAILS_ACTIVITY_CODE)
    }

    private fun showDetailsInSecondPane(itemName: String) {
        fragment = ItemDetailFragment.getInstance(itemName).apply {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.detailsContainer, this, DETAILS_FRAGMENT_TAG)
                .commit()
        }
    }

    private fun removeFragment() {
        supportFragmentManager.findFragmentByTag(DETAILS_FRAGMENT_TAG)?.let {
            supportFragmentManager
                .beginTransaction()
                .remove(it)
                .commit()
        }
    }

    companion object {
        private const val DETAILS_ACTIVITY_CODE = 11
        private const val DETAILS_FRAGMENT_TAG = "detailsFragmentTag"
    }
}
