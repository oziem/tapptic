package com.example.tapptic.list;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.datasource.model.Item;
import com.example.tapptic.App;
import com.example.tapptic.R;

import java.util.List;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

    private List<Item> values;
    private ItemClickListener onItemClick;
    private int selectedPosition = -1;

    public ListAdapter(
        @NonNull List<Item> values,
        @NonNull ItemClickListener onItemClick
    ) {
        this.values = values;
        this.onItemClick = onItemClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
            .inflate(R.layout.item_view_holder, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        viewHolder.onBind(position);
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    /**
     * Mark row with matching item name as selected
     * @param name of Item to select
     * @return position of selected item
     */
    public int setSelectedByName(@NonNull String name) {
        int index = -1;
        for (Item item : values) {
            if (item.getName().equals(name)) {
                index = values.indexOf(item);
                break;
            }
        }
        changeSelected(index);
        return index;
    }

    private void changeSelected(int position) {
        notifyItemChanged(selectedPosition);
        selectedPosition = position;
        notifyItemChanged(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView image;
        private TextView name;

        ViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            name = view.findViewById(R.id.textView);
        }

        void onBind(final int position) {
            Item item = values.get(position);
            name.setText(item.getName());
            checkSelection(itemView, position);
            App.getPicasso(itemView.getContext())
                .load(item.getImageUrl())
                .into(image);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onHolderClick(position);
                }
            });
        }

        private void onHolderClick(int position) {
            onItemClick.onClick(values.get(position).getName());
            changeSelected(position);
        }

        private void checkSelection(View view, int position) {
            if (view.getResources().getBoolean(R.bool.isLandscapeTablet)) {
                view.setSelected(selectedPosition == position);
            }
        }
    }

    interface ItemClickListener {
        void onClick(String itemName);
    }
}

