package com.example.tapptic.details.fragment

import com.example.datasource.repository.ApiError
import com.example.datasource.repository.ApiRepository
import com.example.tapptic.common.MvpPresenter
import io.reactivex.rxkotlin.plusAssign
import javax.inject.Inject

class ItemDetailsFragmentPresenter @Inject constructor(
    private val apiRepository: ApiRepository
) : MvpPresenter<DetailsViewState>() {

    fun fetchItemDetails(itemName: String) {
        compositeDisposable += apiRepository.getItemDetails(itemName)
            .toObservable()
            .map<DetailsViewState> { DetailsViewState.Populated(it.text, it.imageUrl) }
            .onErrorReturn { DetailsViewState.Error(apiRepository.parseError(it)) }
            .startWith(DetailsViewState.Loading)
            .subscribe(statePublisher::onNext)
    }
}

sealed class DetailsViewState {
    object Loading : DetailsViewState()
    data class Error(val error: ApiError) : DetailsViewState()
    data class Populated(val text: String, val imageUrl: String) : DetailsViewState()
}
