package com.example.tapptic.details.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.datasource.repository.ApiError
import com.example.tapptic.App
import com.example.tapptic.R
import com.example.tapptic.common.MvpFragment
import com.example.tapptic.common.requireArguments
import kotlinx.android.synthetic.main.fragment_item_detail.*

class ItemDetailFragment : MvpFragment<DetailsViewState, ItemDetailsFragmentPresenter>() {

    companion object {
        fun getInstance(itemName: String) = ItemDetailFragment().apply {
            arguments = Bundle().apply {
                putString(ITEM_NAME_ARG, itemName)
            }
        }

        fun readDetailsFromState(state: Bundle?) = state?.getString(ITEM_NAME_ARG)

        private const val ITEM_NAME_ARG = "itemNameArg"
    }

    private val component by lazy { App.getComponent(requireContext()).itemDetailsComponent() }

    private val picasso by lazy { App.getComponent(requireContext()).getPicasso() }

    private val itemName by lazy { requireArguments().getString(ITEM_NAME_ARG) }

    override fun createPresenter() = component.getPresenter().apply {
        fetchItemDetails(itemName)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_item_detail, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        retryButton.setOnClickListener { presenter.fetchItemDetails(itemName) }
    }

    override fun onStateChanged(state: DetailsViewState) = when(state) {
        DetailsViewState.Loading -> showLoading()
        is DetailsViewState.Error -> showError(state.error)
        is DetailsViewState.Populated -> populate(state.text, state.imageUrl)
    }

    private fun showLoading(show: Boolean = true) {
        progressBar.visibility =
            if (show) View.VISIBLE
            else View.GONE
    }

    private fun showError(error: ApiError) {
        clearView()
        retryButton.visibility = View.VISIBLE
        textView.text = when (error) {
            ApiError.NoNetworkConnection -> getString(R.string.no_network_connection)
            is ApiError.UnknownError -> getString(R.string.unknown_error, null)
        }
    }

    private fun populate(text: String, imageUrl: String) {
        clearView()
        picasso.load(imageUrl)
            .into(bigImage)
        textView.text = text
    }

    private fun clearView() {
        showLoading(false)
        retryButton.visibility = View.GONE
    }

    fun saveState(bundle: Bundle?) = bundle?.apply {
        itemName?.let { putString(ITEM_NAME_ARG, it) }
    }

    fun getDetailedItemName(): String = itemName
}
