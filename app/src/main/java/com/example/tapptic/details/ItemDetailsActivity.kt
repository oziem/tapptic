package com.example.tapptic.details

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.NavUtils
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.example.tapptic.R
import com.example.tapptic.details.fragment.ItemDetailFragment

class ItemDetailsActivity : AppCompatActivity() {

    companion object {
        fun start(
            activity: Activity,
            itemName: String,
            requestCode: Int
        ) = activity.startActivityForResult(
            Intent(activity, ItemDetailsActivity::class.java).apply {
                putExtra(ITEM_NAME_EXTRA, itemName)
            },
            requestCode
        )

        const val ITEM_NAME_EXTRA = "itemNameExtra"
    }

    private val itemName by lazy { intent.getStringExtra(ITEM_NAME_EXTRA) }

    override fun onCreate(savedInstanceState: Bundle?) {
        if (resources.getBoolean(R.bool.isLandscapeTablet)) {
            val intentt = intent
            setResult(Activity.RESULT_OK, intentt)
            finish()
        }
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item_detail)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        savedInstanceState ?: initFragment()
    }

    private fun initFragment() {
        supportFragmentManager.beginTransaction()
            .add(R.id.rootView, ItemDetailFragment.getInstance(itemName))
            .commit()
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            NavUtils.navigateUpFromSameTask(this)
            true
        }
        else -> super.onOptionsItemSelected(item)
    }
}
