package com.example.tapptic.details.fragment

import com.example.tapptic.di.FragmentScope
import dagger.Subcomponent

@FragmentScope
@Subcomponent
interface ItemDetailsComponent {
    fun getPresenter(): ItemDetailsFragmentPresenter
}
