package com.example.tapptic.di

import android.content.Context
import com.example.tapptic.App
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val app: App) {
    @Provides
    @Singleton
    fun provideContext(): Context = app

    @Provides
    @Singleton // TODO: add lib OkHttp Downloader and add client from DataSourceModule for logs
    fun providePicasso(context: Context): Picasso = Picasso.Builder(context).build()
}
