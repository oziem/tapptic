package com.example.tapptic.di

import com.example.datasource.DataSourceModule
import com.example.tapptic.details.fragment.ItemDetailsComponent
import com.example.tapptic.list.ListActivityComponent
import com.squareup.picasso.Picasso
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    (ApplicationModule::class),
    (DataSourceModule::class)
])
interface AppComponent {

    fun getPicasso() : Picasso

    fun listActivityComponent() : ListActivityComponent

    fun itemDetailsComponent() : ItemDetailsComponent
}
