package com.example.datasource

import android.content.Context
import com.example.datasource.common.NetworkConnectionChecker
import com.example.datasource.repository.ApiRepository
import com.example.datasource.repository.ApiRepositoryImpl
import com.example.datasource.service.ApiService
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
open class DataSourceModule {

    @Provides
    fun provideOkHttpClient(context: Context): OkHttpClient = OkHttpClient.Builder()
        .readTimeout(TIMEOUT_DURATION, TimeUnit.SECONDS)
        .writeTimeout(TIMEOUT_DURATION, TimeUnit.SECONDS)
        .addInterceptor(OkHttpInterceptors.loggingInterceptor())
        .addInterceptor(OkHttpInterceptors.rewriteCacheControlInterceptor(context))
        .cache(Cache(File(context.cacheDir, CACHE_FILE_NAME), CACHE_MAX_SIZE))
        .build()

    @Provides
    fun provideGson(): Gson = GsonBuilder()
        .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        .create()

    @Provides
    @Singleton
    fun provideApiService(gson: Gson, okHttpClient: OkHttpClient): ApiService = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addConverterFactory(ScalarsConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
        .client(okHttpClient)
        .baseUrl(BuildConfig.BASE_URL)
        .build()
        .create(ApiService::class.java)

    @Provides
    fun provideApiRepository(
        apiService: ApiService,
        networkConnectionChecker: NetworkConnectionChecker
    ) : ApiRepository = ApiRepositoryImpl(apiService, networkConnectionChecker)

    companion object {
        private const val TIMEOUT_DURATION = 20L
        private const val CACHE_FILE_NAME = "responses"
        private const val CACHE_MAX_SIZE = 10 * 1024 * 1024L  //10MB
    }
}
