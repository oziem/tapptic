package com.example.datasource.service

import com.example.datasource.model.Item
import com.example.datasource.model.ItemDetails
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("test/json.php")
    fun getItems(): Single<List<Item>>

    @GET("test/json.php")
    fun getItemDetails(@Query("name") name : String): Single<ItemDetails>
}
