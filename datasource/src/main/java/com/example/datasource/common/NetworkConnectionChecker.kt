package com.example.datasource.common

import android.content.Context
import android.net.ConnectivityManager
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NetworkConnectionChecker @Inject constructor(
    private val context: Context
) {
    val isDeviceConnected get() = (context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager)
        .activeNetworkInfo?.isConnected == true
}
