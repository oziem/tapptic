package com.example.datasource

import android.content.Context
import android.net.ConnectivityManager

object NetworkUtils {
  fun isNetworkAvailable(context: Context) =
    (context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).let {
      val activeNetworkInfo = it.activeNetworkInfo
      activeNetworkInfo != null && activeNetworkInfo.isConnected
    }
}
