package com.example.datasource.repository

import com.example.datasource.common.NetworkConnectionChecker
import com.example.datasource.service.ApiService
import javax.inject.Singleton

@Singleton
class ApiRepositoryImpl(
    private val apiService: ApiService,
    private val networkConnectionChecker: NetworkConnectionChecker
) : ApiRepository {

    override fun getItems() = apiService.getItems()

    override fun getItemDetails(name: String) = apiService.getItemDetails(name)

    override fun parseError(throwable: Throwable) =
        if (networkConnectionChecker.isDeviceConnected) {
            ApiError.UnknownError(throwable.message)
        } else {
            ApiError.NoNetworkConnection
        }
}

sealed class ApiError : Exception() {
    object NoNetworkConnection : ApiError()
    data class UnknownError(val info: String?) : ApiError()
}
