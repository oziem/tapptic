package com.example.datasource.repository

import com.example.datasource.model.Item
import com.example.datasource.model.ItemDetails
import io.reactivex.Single

interface ApiRepository {
    fun getItems(): Single<List<Item>>
    fun getItemDetails(name: String): Single<ItemDetails>
    fun parseError(throwable: Throwable): ApiError
}
