package com.example.datasource.model

import com.google.gson.annotations.SerializedName

data class ItemDetails(
    val name: String,
    val text: String,
    @SerializedName("image") val imageUrl: String
)
