package com.example.datasource.model

import com.google.gson.annotations.SerializedName

data class Item(
    val name: String,
    @SerializedName("image") val imageUrl: String
)
