# Tapptic

Application using MVP architecture with a little bit of MVI inspiration. 

It's representing a list of "Items" with an option to view its details.

The logic for handling data is moved to "datasource" module, and integrated with dependency injection graph via `DataSourceModule`.